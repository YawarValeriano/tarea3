package factoryRequest;

public class FactoryRequest {
    public static final String GET = "get";
    public static final String POST = "post";

    public static Request make(String requestType) {
        Request request;

        switch (requestType) {
            case GET:
                request = new RequestGET();
                break;
            case POST:
                request = new RequestPOST();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + requestType);
        }
        return request;
    }
}
