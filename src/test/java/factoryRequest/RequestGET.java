package factoryRequest;

import javax.ws.rs.core.Response;

public class RequestGET extends Request {
    @Override
    public ResponseInformation send(RequestInformation information) {
        ResponseInformation responseInformation = new ResponseInformation();

        Response response = this.client.target(information.getUrl()).request().get();
        System.out.println("INFO > GET " + information.getUrl());

        responseInformation.setBody(response.readEntity(String.class));
        responseInformation.setCode(response.getStatus());

        System.out.println("INFO > GET "+responseInformation.getBody());
        System.out.println("INFO > GET "+responseInformation.getCode());

        response.close();

        return responseInformation;
    }
}
