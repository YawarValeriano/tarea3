package factoryBrowser;

public class FactoryBrowser {
    public static final String FIREFOX_PROXY = "firefox_proxy";
    public static final String CHROME_PROXY = "chrome_driver";

    public static IBrowser make(String browserType) {
        IBrowser browser;
        switch (browserType) {
            case FIREFOX_PROXY:
                browser = new FirefoxProxy();
                break;
            case CHROME_PROXY:
                browser = new ChromeProxy();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + browserType);
        }
        return browser;

    }
}
