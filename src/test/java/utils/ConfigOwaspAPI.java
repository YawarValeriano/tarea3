package utils;

public class ConfigOwaspAPI {

    public static final String HOST_AND_PORT = ConfigEnv.OWASP_HOST + ConfigEnv.PORT;
    public static final String START_SCANNER = HOST_AND_PORT + "/JSON/ascan/action/scan/?url=";
    public static final String GET_PROGRESS = HOST_AND_PORT + "/JSON/ascan/view/status/?scanId=";
    public static final String GET_ATTACKS = HOST_AND_PORT + "/HTML/ascan/view/scanProgress/?scanId=";
    public static final String GET_REPORT = HOST_AND_PORT + "/OTHER/core/other/htmlreport/?";
}
