package utils;

public class ConfigEnv {
    public static final String OWASP_HOST = "http://localhost";
    public static final String PORT = ":8888";

    public static final String DESTINY_HOST = "https://todoist.com/";
}
