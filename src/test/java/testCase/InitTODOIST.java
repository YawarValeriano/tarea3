package testCase;

import io.qameta.allure.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pageObject.MainPage;
import session.Session;
import utils.ConfigEnv;

public class InitTODOIST {
    MainPage mainPage = new MainPage();

    @BeforeEach
    public void before() {
        Session.getInstance().getDriver().get(ConfigEnv.DESTINY_HOST);
    }

    @DisplayName("Verify init execution")
    @Description("Test to verify...")
    @Link(ConfigEnv.DESTINY_HOST)
    @Severity(SeverityLevel.BLOCKER)
    @Test
    public void verifyLoginInPage() {
        mainPage.startButton.click();
    }

    @AfterEach
    public void after() {
        Session.getInstance().closeSession();
    }
}
