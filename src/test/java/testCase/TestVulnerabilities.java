package testCase;

import factoryRequest.FactoryRequest;
import factoryRequest.RequestInformation;
import factoryRequest.ResponseInformation;
import io.qameta.allure.*;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import utils.ConfigEnv;
import utils.ConfigOwaspAPI;

public class TestVulnerabilities {
    private String idScanner;

    @DisplayName("Verify vulnerabilities")
    @Description("Test to verify...")
    @Link(ConfigEnv.DESTINY_HOST)
    @Issue("Bug00001")
    @Severity(SeverityLevel.CRITICAL)
    @Test
    public void testVulnerabilitiesInTODOIST() {
        this.startScanner();
        this.monitoring();
    }

    @Step("Empezando el escaneo de vulnerabilidades con ZAP")
    public void startScanner() {
        RequestInformation information = new RequestInformation();
        information.setUrl(ConfigOwaspAPI.START_SCANNER + ConfigEnv.DESTINY_HOST);

        ResponseInformation responseInformation= FactoryRequest.make(FactoryRequest.GET).send(information);
        JSONObject body = new JSONObject(responseInformation.getBody());
        idScanner = body.get("scan").toString();
    }

    @Step("Monitoreando hasta que llegar al 100%")
    public void monitoring() {
        String percentage = "0";
        RequestInformation information = new RequestInformation();
        information.setUrl(ConfigOwaspAPI.GET_PROGRESS + idScanner);

        while(!percentage.equals("100")){
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ResponseInformation responseInformation = FactoryRequest.make(FactoryRequest.GET).send(information);
            JSONObject body = new JSONObject(responseInformation.getBody());
            percentage = body.get("status").toString();
            System.out.println("STATUS ----> "+percentage);
        }
    }

    @Attachment(value = "{0}", type = "text/html")
    public String attachmentFile(String name,String content){
        return content;
    }

    @AfterEach
    public void after() {
        RequestInformation information = new RequestInformation();
        information.setUrl(ConfigOwaspAPI.GET_REPORT);
        ResponseInformation responseInformation = FactoryRequest.make(FactoryRequest.GET).send(information);
        this.attachmentFile("OWASP ZAP REPORT",responseInformation.getBody());

        information.setUrl(ConfigOwaspAPI.GET_ATTACKS + idScanner);
        responseInformation = FactoryRequest.make(FactoryRequest.GET).send(information);
        this.attachmentFile("Lista De Vulnerabilidades",responseInformation.getBody());
    }
}
